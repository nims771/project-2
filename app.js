const App = {
    data(){
        return{
            style1 : {
                backgroundColor:"rgb(190, 191, 195)",
                width:"185px",
                top: "0px",
                },
                style2: {
                    backgroundColor:"rgb(190, 191, 195)",
                    width:"125px",
                    bottom: "0px",
                }
        }
    },
    methods: {
        hoverIn(){
            this.style1.backgroundColor = "rgb(228, 86, 66)";
            this.style2.backgroundColor = "rgb(231, 158, 143)";
            this.style1.top = "80px";
            this.style2.bottom = "95px";
        },
        hoverOut(){
            this.style1.backgroundColor = "rgb(190, 191, 195)";
            this.style2.backgroundColor = "rgb(190, 191, 195)";
            this.style1.top = "0px";
            this.style2.bottom = "0px";
        },
    },
    mounted() {
    },
    computed: {},

};

Vue.createApp(App).mount('#app');